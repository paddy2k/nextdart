typedef struct Station {
    char *name;
} Station;

Station *station_create(const char *name);

void station_destroy(Station *station);