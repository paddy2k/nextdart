typedef struct Train {
    char *destination;
    char *due;
} Train;

Train *train_create(const char *destination, int due);

void train_destroy(Train *train);