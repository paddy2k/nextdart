#include <pebble.h>
#include "train.h"

Train *train_create(const char *destination, int due)
{
    Train *train = malloc(sizeof(Train));
    
    int destination_size = strlen(destination) + 1;    
    train->destination = malloc(sizeof(char) * destination_size);
    strncpy(train->destination, destination, destination_size);
    
    static const int due_size = 4;
    train->due = malloc(sizeof(char) * due_size);
    if (due == 0) {
        strncpy(train->due, "Due", due_size);
    } else {
        snprintf(train->due, due_size, "%d", due);
    }
    return train;
}

void train_destroy(Train *train)
{
    if (!train) {
        return;
    }
    free(train->destination);
    free(train->due);
    free(train);
}