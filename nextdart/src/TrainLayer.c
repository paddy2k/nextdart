#include <Pebble.h>
#include "TrainLayer.h"
#include "Train.h"

/*

  Train layer layout:

  +--------------124--------------+
  |+--------89-------+ 10 +--25--+|
 22|      Destination|    |Due   ||
  |+-----------------+    +------+|
  +-------------------------------+

 */

#define TRAIN_LAYER_WIDTH 124
#define TRAIN_LAYER_HEIGHT 22
#define DUE_TEXT_LAYER_FRAME GRect(99, 0, 25, 22)
#define DESTINATION_TEXT_LAYER_FRAME GRect(0, 0, 89, 22)

TrainLayer *train_layer_create(GRect frame)
{
    TrainLayer *train_layer = malloc(sizeof(TrainLayer));
    
    train_layer->root_layer = layer_create(GRect(frame.origin.x, frame.origin.y, TRAIN_LAYER_WIDTH, TRAIN_LAYER_HEIGHT));
    
    train_layer->due_text_layer = text_layer_create(DUE_TEXT_LAYER_FRAME);
    train_layer->destination_text_layer = text_layer_create(DESTINATION_TEXT_LAYER_FRAME);
    
    text_layer_set_font(
        train_layer->due_text_layer, 
        fonts_get_system_font(FONT_KEY_GOTHIC_18_BOLD)
    );
    
    text_layer_set_font(
        train_layer->destination_text_layer, 
        fonts_get_system_font(FONT_KEY_GOTHIC_18)
    );
    
    text_layer_set_text_alignment(
        train_layer->due_text_layer, 
        GTextAlignmentRight
    );
    
    text_layer_set_text_color(
        train_layer->due_text_layer, 
        GColorWhite
    );
    
    text_layer_set_text_color(
        train_layer->destination_text_layer, 
        GColorWhite
    );    
    
    text_layer_set_background_color(
        train_layer->due_text_layer, 
        GColorBlack
    );
    
    text_layer_set_background_color(
        train_layer->destination_text_layer, 
        GColorBlack
    );
    
    layer_add_child(
        train_layer->root_layer, 
        (Layer *)train_layer->due_text_layer
    );
        
    layer_add_child(
        train_layer->root_layer, 
        (Layer *)train_layer->destination_text_layer
    );
    
    return train_layer;
}

void train_layer_destroy(TrainLayer *train_layer)
{    
    text_layer_destroy(train_layer->due_text_layer);
    text_layer_destroy(train_layer->destination_text_layer);
    layer_destroy(train_layer->root_layer);
    
    free(train_layer);
}

void train_layer_set_train(TrainLayer *train_layer, Train *train)
{
    train_layer->train = train;
    
    if (train) {
        text_layer_set_text(
            train_layer->due_text_layer, 
            train->due
        );
        text_layer_set_text(
            train_layer->destination_text_layer, 
            train->destination
        );
    } else {
        text_layer_set_text(
            train_layer->due_text_layer, 
            ""
        );
        text_layer_set_text(
            train_layer->destination_text_layer, 
            ""
        );
    }
}

Layer *train_layer_get_layer(TrainLayer *train_layer)
{
    return train_layer->root_layer;
}