#include <pebble.h>

#include "station.h"
#include "train.h"
#include "TrainLayer.h"

enum {
    STATUS_OK,
    STATUS_NO_INTERNET,
    STATUS_SERVER_ERROR,
    STATUS_NO_LOCATION,
};

enum {
    STATUS_KEY,
    STATION_KEY,
    NORTHBOUND_TRAINS_KEY,
    SOUTHBOUND_TRAINS_KEY,
};

static const uint32_t FetchKey = 1;
static const uint32_t FetchValue = 1;

static Station *station;
static Train *northbound_trains[3] = {};
static Train *southbound_trains[3] = {};

static Window *window;

static TextLayer *info_text_layer;

static TextLayer *station_text_layer;
static TrainLayer *northbound_train_layers[3] = {};
static TrainLayer *southbound_train_layers[3] = {};

static TextLayer *no_northbound_trains_text_layer;
static TextLayer *no_southbound_trains_text_layer;
static TextLayer *no_trains_text_layer;

static GBitmap *separator;
static BitmapLayer *separator_layer;

static void show_info()
{
    layer_set_hidden((Layer *)info_text_layer, false);
}

static void hide_info()
{
    layer_set_hidden((Layer *)info_text_layer, true);
}

static void show_no_northbound_trains()
{
    layer_set_hidden((Layer *)no_northbound_trains_text_layer, false);
}

static void hide_no_northbound_trains()
{
    layer_set_hidden((Layer *)no_northbound_trains_text_layer, true);
}

static void show_no_southbound_trains()
{
    layer_set_hidden((Layer *)no_southbound_trains_text_layer, false);
}

static void hide_no_southbound_trains()
{
    layer_set_hidden((Layer *)no_southbound_trains_text_layer, true);
}


static void show_no_trains()
{
    layer_set_hidden((Layer *)no_northbound_trains_text_layer, true);
    layer_set_hidden((Layer *)no_southbound_trains_text_layer, true);
    layer_set_hidden((Layer *)no_trains_text_layer, false);
}

static void hide_no_trains()
{
    layer_set_hidden((Layer *)no_trains_text_layer, true);
}


static void update_window()
{
    bool have_northbound_trains = false;
    bool have_southbound_trains = false;
    // Update station name
    text_layer_set_text(station_text_layer, station->name);
    
    // Update northbound trains
    for (int i = 0; i < 3; i++) {
        train_layer_set_train(northbound_train_layers[i], northbound_trains[i]);
        if (northbound_trains[i]) {
            have_northbound_trains = true;
        }
    }
    
    if (have_northbound_trains) {
        hide_no_northbound_trains();
    } else {
        show_no_northbound_trains();
    }
    
    // Update southbound trains
    for (int i = 0; i < 3; i++) {
        train_layer_set_train(southbound_train_layers[i], southbound_trains[i]);
        if (southbound_trains[i]) {
            have_southbound_trains = true;
        }
    }
    
    if (have_southbound_trains) {
        hide_no_southbound_trains();
    } else {
        show_no_southbound_trains();
    }
    
    if (!have_northbound_trains && !have_southbound_trains) {
        show_no_trains();
    } else {
        hide_no_trains();        
    }
}

static void fetch_train_data(void *timerData)
{
    DictionaryIterator *dictionary;
    app_message_outbox_begin(&dictionary);
    dict_write_uint32(dictionary, FetchKey, FetchValue);
    app_message_outbox_send();
}

static void update_trains_using_data(Train **trains, Tuple *train_data)
{
    Train *train;
    int index = 0;
    uint8_t val;
    uint8_t due;
    char destination[20];
    int destination_index = 0;
    bool is_in_destination_string = false;
    
    for (uint8_t i = 0; i < train_data->length; i++) {
        val = train_data->value->data[i];
        if (is_in_destination_string) {
            if (destination_index < 20) {
                destination[destination_index] = val;
            } else {
                destination[19] = 0;
            }
            if (val == 0) {
                is_in_destination_string = false;
                
                train = train_create(
                    destination, 
                    due
                );
                
                trains[index] = train;
                index++;
                
                destination_index = 0;
                destination[destination_index] = 0;
            } else {
                destination_index++;
            }
        } else {
            due = val;
            is_in_destination_string = true;                     
        }                    
    }
}

static void out_sent_handler(DictionaryIterator *sent, void *context)
{
    
}

static void out_failed_handler(DictionaryIterator *failed, AppMessageResult reason, void *context)
{
    text_layer_set_text(station_text_layer, "ERROR");
    text_layer_set_text(info_text_layer, "\n\nBluetooth error\n:(");
    show_info();
}

static void in_received_handler(DictionaryIterator *received, void *context)
{    
    station_destroy(station);
    
    for (int i = 0; i < 3; i++) {
        train_destroy(northbound_trains[i]);
        northbound_trains[i] = NULL;
    }
    
    for (int i = 0; i < 3; i++) {
        train_destroy(southbound_trains[i]);
        southbound_trains[i] = NULL;
    }
    
    Tuple *status_data = dict_find(received, STATUS_KEY);
    int32_t status = status_data->value->int32;
    
    switch (status) {
        case STATUS_OK: {
            
            Tuple *station_data = dict_find(received, STATION_KEY);
            station = station_create(station_data->value->cstring);
            
            Tuple *northbound_trains_data = dict_find(received, NORTHBOUND_TRAINS_KEY);
            Tuple *southbound_trains_data = dict_find(received, SOUTHBOUND_TRAINS_KEY);
            
            if (northbound_trains_data) {
                update_trains_using_data(northbound_trains, northbound_trains_data);
            }
            
            if (southbound_trains_data) {
                update_trains_using_data(southbound_trains, southbound_trains_data);
            }
            
            hide_info();
            
            update_window();
            
            break;
        }
        case STATUS_NO_INTERNET: {
            text_layer_set_text(station_text_layer, "ERROR");
            text_layer_set_text(info_text_layer, "\n\nNo internet connection\n:(");
            show_info();
            break;
        }
        case STATUS_SERVER_ERROR: {
            text_layer_set_text(station_text_layer, "ERROR");
            text_layer_set_text(info_text_layer, "\n\nServer error\n:(");
            show_info();
            break;
        }
        case STATUS_NO_LOCATION: {
            text_layer_set_text(station_text_layer, "ERROR");
            text_layer_set_text(info_text_layer, "\n\nCouldn’t establish your location\n:(");
            show_info();
            break;
        }
    }
    
    app_timer_register(30000, fetch_train_data, NULL);
}

static void in_dropped_handler(AppMessageResult reason, void *context)
{
    text_layer_set_text(station_text_layer, "ERROR");
    text_layer_set_text(info_text_layer, "\n\nBluetooth error\n:(");
    show_info();
}

static void window_load(Window *window)
{
    Layer *window_layer = window_get_root_layer(window);
    
    window_set_background_color(window, GColorBlack);
    
    station_text_layer = text_layer_create(GRect(0, 0, 144, 24));

    text_layer_set_font(
        station_text_layer, 
        fonts_get_system_font(FONT_KEY_GOTHIC_18_BOLD)
    );
    
    text_layer_set_text_color(station_text_layer, GColorBlack);
	text_layer_set_background_color(station_text_layer, GColorWhite);

	text_layer_set_text_alignment(
        station_text_layer, 
        GTextAlignmentCenter
    );

    layer_add_child(window_layer, (Layer *)station_text_layer);
    
    // Northbound train layers    
    for (int i = 0; i < 3; i++) {
        northbound_train_layers[i] = train_layer_create(GRect(10, (24 + 23 * i), 0, 0)); 
        layer_add_child(window_layer, train_layer_get_layer(northbound_train_layers[i]));        
    }
    
    no_northbound_trains_text_layer = text_layer_create(GRect(0, 47, 144, 24));
    text_layer_set_font(no_northbound_trains_text_layer, fonts_get_system_font(FONT_KEY_GOTHIC_18));
    text_layer_set_text_color(no_northbound_trains_text_layer, GColorWhite);
	text_layer_set_background_color(no_northbound_trains_text_layer, GColorBlack);
	text_layer_set_text_alignment(no_northbound_trains_text_layer, GTextAlignmentCenter);

    layer_add_child(window_layer, (Layer *)no_northbound_trains_text_layer);
    text_layer_set_text(no_northbound_trains_text_layer, "No northbound trains");
    hide_no_northbound_trains();

    separator = gbitmap_create_with_resource(RESOURCE_ID_IMAGE_SEPARATOR);
    
    separator_layer = bitmap_layer_create(GRect(0, 96, 144, 1));
    bitmap_layer_set_bitmap(separator_layer, separator);
    layer_add_child(window_layer, (Layer *)separator_layer); 
    
    // Southbound train layers    
    for (int i = 0; i < 3; i++) {
        southbound_train_layers[i] = train_layer_create(GRect(10, (97 + 23 * i), 0, 0));
        layer_add_child(window_layer, train_layer_get_layer(southbound_train_layers[i]));        
    }
    
    no_southbound_trains_text_layer = text_layer_create(GRect(0, 120, 144, 24));
    text_layer_set_font(no_southbound_trains_text_layer, fonts_get_system_font(FONT_KEY_GOTHIC_18));
    text_layer_set_text_color(no_southbound_trains_text_layer, GColorWhite);
	text_layer_set_background_color(no_southbound_trains_text_layer, GColorBlack);
	text_layer_set_text_alignment(no_southbound_trains_text_layer, GTextAlignmentCenter);

    layer_add_child(window_layer, (Layer *)no_southbound_trains_text_layer);
    text_layer_set_text(no_southbound_trains_text_layer, "No southbound trains"); 
    hide_no_southbound_trains();
    
    // No trains layer
    no_trains_text_layer = text_layer_create(GRect(0, 84, 144, 24));
    text_layer_set_font(no_trains_text_layer, fonts_get_system_font(FONT_KEY_GOTHIC_18));
    text_layer_set_text_color(no_trains_text_layer, GColorWhite);
	text_layer_set_background_color(no_trains_text_layer, GColorBlack);
	text_layer_set_text_alignment(no_trains_text_layer, GTextAlignmentCenter);
    layer_add_child(window_layer, (Layer *)no_trains_text_layer);
    text_layer_set_text(no_trains_text_layer, "No trains"); 
    hide_no_trains();
    
    // Info layer    
    info_text_layer = text_layer_create(GRect(0, 24, 144, 144));
    text_layer_set_font(info_text_layer, fonts_get_system_font(FONT_KEY_GOTHIC_18_BOLD));
    text_layer_set_text_color(info_text_layer, GColorWhite);
	text_layer_set_background_color(info_text_layer, GColorBlack);
	text_layer_set_text_alignment(info_text_layer, GTextAlignmentCenter);

    layer_add_child(window_layer, (Layer *)info_text_layer);
    text_layer_set_text(info_text_layer, "\n\nFetching data...");
    show_info();
}

static void window_unload(Window *window)
{
    
    text_layer_destroy(station_text_layer);
    
    // Northbound train layers    
    for (int i = 0; i < 3; i++) {
        train_layer_destroy(northbound_train_layers[i]);      
    }
    
    bitmap_layer_destroy(separator_layer);
    gbitmap_destroy(separator);
    
    // Southbound train layers    
    for (int i = 0; i < 3; i++) {
        train_layer_destroy(southbound_train_layers[i]);
    }
    
    text_layer_destroy(no_northbound_trains_text_layer);
    text_layer_destroy(no_southbound_trains_text_layer);
    text_layer_destroy(no_trains_text_layer);
    
    text_layer_destroy(info_text_layer);
}

static void init(void)
{
    // Set up window
    window = window_create();
    
    window_set_fullscreen(window, true);
    
    window_set_window_handlers(window, (WindowHandlers) {
        .load = window_load,
        .unload = window_unload,
    });
    
    const bool animated = true;
    window_stack_push(window, animated);    

    // Set up connection to phone
    app_message_register_inbox_received(in_received_handler);
    app_message_register_inbox_dropped(in_dropped_handler);
    app_message_register_outbox_sent(out_sent_handler);
    app_message_register_outbox_failed(out_failed_handler);

    app_message_open(app_message_inbox_size_maximum(), dict_calc_buffer_size(1, sizeof(FetchValue)));
}

static void deinit(void)
{
    window_destroy(window);
}

int main(void)
{
    init();    
    app_event_loop();    
    deinit();
}
