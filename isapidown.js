#!/usr/bin/env node

"use strict";

var http = require("http"),
    red = 1,
    yellow = 3,
    blue = 6,
    colorText = function (text, color) { return "\x1b[3" + color + "m" + text + "\x1b[0m"; },
    errorPrefix = colorText("[error]", red),
    warningPrefix = colorText("[warning]", yellow),
    infoPrefix = colorText("[info]", blue);

http.get("http://api.irishrail.ie/realtime/realtime.asmx/getCurrentTrainsXML", function (res) {
    if (res.statusCode !== 200) {
        console.log(errorPrefix, "API is down with status code", res.statusCode);
    } else {
        if (res.headers["content-length"] === "209") { // 209 is the number of characters in an empty XML response from the API            
            console.log(warningPrefix, "API is returning no data");
        } else {
            console.log(infoPrefix, "API is operating normally");
        }
    }
    process.exit();
}).on("error", function (e) {
    console.log(errorPrefix, e.message);
    process.exit();
});